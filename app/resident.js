app.controller('residentCtrl', function($scope, $http, $mdToast, moment) {
    
    $scope.residents = [
        {
            id: 1,
            firstName: "Tim",
            lastName: "Rogers",
            duties: [
                {
                    id: 1,
                    start_time: "2016-11-08T06:30:45.505Z",
                    end_time: "2016-11-09T06:31:00.000Z"
                },
                {
                    id: 2,
                    start_time: "2016-11-09T06:30:45.505Z",
                    end_time: "2016-11-10T06:31:00.000Z"
                }
            ]
        }    
    ];
    
    // Get a single resident
    $scope.getResident = function(res) {
        $scope.res = $scope.residents[res];
        $scope.res_index = res;
        $scope.resName = $scope.res.firstName + " " + $scope.res.lastName;
    };
    
    
    // Add a Resident
    $scope.addResident = function(resident) {
        $scope.resident.id = $scope.residents.length + 1;
        $scope.resident.duties = [];
        
        $scope.residents.push(resident);
        console.log(JSON.stringify($scope.residents));
        $scope.toast(resident.firstName + ' ' + resident.lastName + ' has been added!');
        $scope.resident = "";
    };
    
    // Add Duty Hours to a Resident
    $scope.addDuties = function(res) {
        $scope.duty = {
            id: $scope.res.duties.length + 1,
            start_time: res.duties.start_time,
            end_time: res.duties.end_time
        };
        
        $scope.residents[$scope.res_index].duties.push($scope.duty);
        
        $scope.toast('Duty Hours Added for ' + res.firstName + ' ' + res.lastName +'!');
        $scope.res = "";
        $scope.resName = "";
        $scope.duty = "";
        console.log(JSON.stringify($scope.residents));
    };
    
    // Show status message
    $scope.toast = function(message) {
        $mdToast.show({
          hideDelay   : 3000,
          position    : 'top right',
          controller  : 'ToastCtrl',
          templateUrl : 'partials/toast.html',
          locals      : {
              message : message
          }
        });
    }
    
    // Set default date and time values
    $scope.date = new Date();
    $scope.time = new Date();
    $scope.dateTime = new Date();
    $scope.minDate = moment().subtract(1, 'month');
    $scope.maxDate = moment().add(1, 'month');
    
    
    
    
    
    
    
    
    // var date = new Date();
    // var d = date.getDate();
    // var m = date.getMonth();
    // var y = date.getFullYear();
    
    // 
});
