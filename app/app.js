var app = angular.module('MyApp', ['ngRoute', 'ngMaterial', 'ngMessages', 'ngMaterialDatePicker', 'angularMoment', 'ui.calendar']);

app.config(['$routeProvider', 
    function($routeProvider) {
        $routeProvider
        .when('/', {
            title: "Resident Duty",
            templateUrl: "partials/resident.html",
            controller: "residentCtrl",
            role: 0
        })
        .when('/admin', {
            title: "Administration",
            templateUrl: "partials/admin.html",
            controller: "adminCtrl"
        })
        .otherwise({
            redirectTo: '/'
        });
    }
])
.directive('dutyHours', function(moment) {
    return {
        restrict: 'E',
        scope: {
            times: '@'
        },
        link: function(scope, element, attrs) {
            scope.duty = scope.$eval(scope.times);
            scope.hours = 0;
    
            angular.forEach(scope.duty, function(value) {
                var start = moment(value.start_time);
                var end = moment(value.end_time);
                scope.hours += end.diff(start, 'hours')
                console.log(scope.hours);                
            });
        },
        template: '{{hours}}'
    }
});